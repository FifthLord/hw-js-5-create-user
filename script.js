

function createNewUser() {
   const newUser = {};

   Object.defineProperties(newUser, {
      _firstName: {
         value: prompt("Введіть ваше Ім'я"),
         writable: false,
         configurable: true
      },
      _lastName: {
         value: prompt("Введіть ваше Прізвище"),
         writable: false,
         configurable: true
      }
   });

   newUser.getLogin = function () {
      return ((this._firstName[0] + this._lastName).toLowerCase());
   }
   newUser.setFirstName = function (newFirstName) {
      Object.defineProperty(newUser, '_firstName', {
         value: newFirstName,
      })
   }
   newUser.setLastName = function (newLastName) {
      Object.defineProperty(newUser, '_lastName', {
         value: newLastName,
      })
   }
   return newUser;
}


const newUser = createNewUser();
console.log(newUser.getLogin());


//-------TEST--------//

// newUser._firstName = "TEST1_1";
// newUser._lastName = "TEST2_1";
// console.log(newUser);

// newUser.setFirstName("TEST1_2");
// newUser.setLastName("TEST2_2");
// console.log(newUser);